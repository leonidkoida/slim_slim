<?php
declare(strict_types = 1);

namespace App\Base;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

abstract class Controller
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Controller constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * @param $service
     * @return mixed
     */
    public function getService(string $service)
    {
        return $this->getContainer()->{$service};
    }

    /**
     * @return Twig
     */
    public function view()
    {
        return $this->getService('view');
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
       return $this->getContainer()->request;
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->getContainer()->response;
    }

    /**
     * @param array $data
     * @return object
     */
    public function json(array  $data = []): object
    {
        return $this->getResponse()->withJson($data);
    }

    /**
     * @param string $template
     * @param array $data
     * @return ResponseInterface
     */
    public function render(string $template, array $data = []): ResponseInterface
    {
        return $this->view()->render($this->getResponse(), $template, $data);
    }
}