<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Base\Controller;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;

class DefaultController extends Controller
{
    /**
     * @return Response
     */
    public function index(): ResponseInterface
    {
        return $this->render('pages/home.html.twig');
    }

    /**
     * @return ResponseInterface
     */
    public function login(): ResponseInterface
    {
        return $this->render('pages/login.html.twig');
    }

    /**
     * @return ResponseInterface
     */
    public function register(): ResponseInterface
    {
        return$this->render('pages/register.html.twig');
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return Response
     */
    public function home(RequestInterface $request, ResponseInterface $response, array $args = []): ResponseInterface
    {
        return $this->render('pages/test.html.twig', ['name' => 'Leonid', 'job' => 'developer', 'user_id' => $args['id']]);
    }
}