<?php

use App\Controller\DefaultController;

$app->get('/', DefaultController::class . ':index')->setName('index');

$app->get('/login', DefaultController::class . ':login')->setName('login');

$app->get('/register', DefaultController::class . ':register')->setName('register');

$app->get('/home/{id}', DefaultController::class . ':home')->setName('home');