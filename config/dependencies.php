<?php
declare(strict_types = 1);

use Slim\Container;

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];

    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function (Container $container) {
    $settings = $container->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));

    return $logger;
};
// twig view
//TODO we need to set correct autoreload condition in dependence of which env we are using(dev/, test(cli), or prod/staging)
$container['view'] = function (Container $container) {
    $view = new Slim\Views\Twig(__DIR__. '/../templates', [
        'strict_variables' => true,
        'cache' => __DIR__ . '/../var/twig/',
        'auto_reload' => getenv('APP_ENV') !== 'production' ? true : false,
    ]);

    $basePath = rtrim(str_replace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));
    $view->addExtension(new Twig_Extension_Debug());

    return $view;
};