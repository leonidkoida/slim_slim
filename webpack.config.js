const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const miniCssExtractPlugin = new MiniCssExtractPlugin({
  filename: './assets/css/style.css'
});

module.exports = {
  entry: './assets/js/app.js',
  output: {
    path: path.resolve(__dirname, './public'),
    filename: './assets/js/app.js',
    publicPath: '/'
  },
  watch: true,
  watchOptions: {
    poll: true,
    ignored: /node_modules/
  },
  devtool: 'cheap-module-source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env']
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {loader: 'css-loader'},
          {loader: 'postcss-loader',
            options: {
              plugins: () => [require('autoprefixer')],
            }},
          {loader: 'sass-loader'}
        ]
      },
      {
        test: /\.(ttf|eot|woff|woff2|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 50000,
              // name: './fonts/[hash].[ext]'
              name: '[name].[ext]',
              outputPath: './assets/fonts/'
            }
          }
        ]
      },
    ]
  },
  plugins: [
    miniCssExtractPlugin
  ]
};