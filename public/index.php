<?php
declare(strict_types = 1);
require __DIR__ . '/../vendor/autoload.php';

session_start();

$settings = require __DIR__ . '/../config/settings.php';

$app = new \Slim\App($settings);

require __DIR__ . '/../config/dependencies.php';
require __DIR__ . '/../config/middleware.php';
require __DIR__ . '/../config/routes.php';

try {
    $app->run();
} catch (\Slim\Exception\MethodNotAllowedException $e) {
} catch (\Slim\Exception\NotFoundException $e) {
    echo($e->getMessage()); // todo this should be handled by monolog
} catch (Exception $e) {
    echo($e->getMessage()); // todo this should be handled by monolog
}
