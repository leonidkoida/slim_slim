import $ from 'jquery';
import 'bootstrap';
import 'jquery.mmenu';
import '../scss/style.scss';

$(document).ready(function () {

  const $menu = $("#main-nav").mmenu({
    "setSelected": {
      "hover": true,
      "parent": true
    },
    "extensions": [
      "pagedim-black",
      "theme-dark",
      "shadow-page",
      "shadow-panels"
    ],
    "navbars": [
      {
        "position": "top",
        "content": [
          "searchfield"
        ]
      }
    ],
    "searchfield": {
      "search": false
    }
  }, {
    clone: true,
    // "searchfield": {
    //   "clear": true
    // }
  });

  const $icon = $("#main-nav_toggle");
  const API = $menu.data( "mmenu" );

  $icon.on( "click", function() {
    API.open();
  });

  API.bind( "open:finish", function() {
    setTimeout(function() {
      $icon.addClass( "is-active" );
    }, 100);
  });
  API.bind( "close:finish", function() {
    setTimeout(function() {
      $icon.removeClass( "is-active" );
    }, 100);
  });


});